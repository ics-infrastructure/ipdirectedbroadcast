#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup


def readme():
    with open("README.md") as f:
        return f.read()


setup(
    name="ipdirectedbroadcast",
    version="0.0.3",
    description="Python script simulates EPICS v3 channel access protocol.",
    long_description=readme(),
    url="https://gitlab.esss.lu.se/ics-infrastructure/ipdirectedbroadcast",
    author="Remy Mudingay",
    author_email="remy.mudingay@ess.eu",
    license="MIT",
    scripts=["bin/ipdirectedbroadcast.py"],
    install_requires=[
        "argparse",
        "multiprocess",
    ],
    test_suite="tests",
    include_package_data=True,
    zip_safe=False,
)
