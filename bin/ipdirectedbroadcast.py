#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import sys
import socket
from time import sleep
import multiprocess as mp
import time

try:
    import argparse
except Exception:
    print("Please install the dependency first")
    print("You can install them by typing pip install -r requirements.txt\n")
    exit()


# Check the current version of python, if not python 3.x, exit.
if sys.version_info[0] != 3:
    print("This script must be run with Python version 3.x")
    exit()


def main():
    # create an argument parser
    # create an argument parser
    parser = argparse.ArgumentParser()

    # add the arguments
    parser.add_argument(
        "--addr",
        help="The destination broadcast address, example 172.168.0.255",
        required=True,
    )
    parser.add_argument(
        "--port",
        type=int,
        default=5046,
        help="The destination port (defaults to 5046)",
    )
    parser.add_argument(
        "--count",
        type=int,
        default=1000,
        help="The number of datagrams to send (defaults to 1000)",
    )
    parser.add_argument(
        "--rate",
        type=int,
        default=14,
        help="The rate in hertz (defaults to 14)",
        choices=range(1, 100),
        metavar="(1..100)",
    )
    parser.add_argument(
        "--size",
        type=int,
        default=80,
        help="The payload size (defaults to 80)",
        choices=range(50, 400),
        metavar="(50..400)",
    )

    # parse the arguments
    args = parser.parse_args()

    # get the command line arguments
    bcAddress = args.addr
    bcPort = args.port
    bcCount = args.count
    bcRate = args.rate
    bcSize = args.size

    def sendUDP(myAddress):
        """Send UDP datagram"""
        b = mp.current_process()
        # assemble payload see bcSize
        payload = b""
        for x in range(0, bcSize):
            payload += b"X"
        s = time.time()
        rateS = 1 / float(bcRate)
        print(
            "Sending "
            + str(bcCount)
            + " UDP segments to "
            + str(bcAddress)
            + ":"
            + str(bcPort)
            + " PID("
            + str(b.pid)
            + ") every "
            + str(rateS)
            + "s"
        )
        # create socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        # send at specified rate
        for x in range(bcCount):
            try:
                sock.sendto(payload, (myAddress, bcPort))
                sleep(rateS)
            except socket.error as e:
                print("Error receiving data: %s" % e)
                sys.exit(1)

    p = mp.Process(target=sendUDP, args=(bcAddress,)).start()


if __name__ == "__main__":
    main()
