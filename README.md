# ipdirectedbroadcast

ipdirectedbroadcast is a Python script that will help simulate EPICS v3 channel access protocol ip directed broadcast. It is a tool that can be used to stress test a network, especially one with ip directed broadcast enabled.

## Install

```bash
git clone https://gitlab.esss.lu.se/ics-infrastructure/ipdirectedbroadcast
cd ipdirectedbroadcast
pip3 install . # doing this in a virtual environment is better

```

## Usage
```sh
usage:
	ipdirectedbroadcast.py -c COUNT -d DESTINATION -p PORT -r RATE -s SIZE

optional arguments:
  -h, --help        show this help message and exit
  -c   COUNT        Number of datagrams/packets to send.
  -d   DESTINATION  Broadcast address. Expects an IPv4 broadcast address.
  -p   PORT         EPICS UDP port. Default is port 5064.
  -r   RATE         Hertz (rate).
  -s   SIZE         Payload size (bytes).

For Example:

       ipdirectedbroadcast.py -c 100000 -d 192.168.15.255 -p 5064 -r 14 -s 80
```
## Uninstall
```sh
pip3 uninstall ipdirectedbroadcast
Found existing installation: ipdirectedbroadcast 0.0.3
Uninstalling ipdirectedbroadcast-0.0.3:
  Would remove:
    /Library/Frameworks/Python.framework/Versions/3.8/bin/ipdirectedbroadcast
    /Library/Frameworks/Python.framework/Versions/3.8/lib/python3.8/site-packages/ipdirectedbroadcast-0.0.3-py3.8.egg-info
Proceed (y/n)? y
  Successfully uninstalled ipdirectedbroadcast-0.0.3
```
[![Quality Gate Status](https://sonarqube.esss.lu.se/api/project_badges/measure?project=ipdirectedbroadcast&metric=alert_status)](https://sonarqube.esss.lu.se/dashboard?id=ipdirectedbroadcast)
